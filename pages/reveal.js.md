---
theme: seriph
background: https://images.unsplash.com/photo-1602992708529-c9fdb12905c9?q=80&w=1470&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D
class: text-center
highlighter: shiki
lineNumbers: false
drawings:
  persist: false
defaults:
  foo: true
transition: slide-left
title: How devs make a pitch deck
mdc: true
---

# How devs make a pitch deck

<div class="abs-br m-6 flex gap-2">
  <a href="https://codeberg.org/ridhof/slides" target="_blank" alt="Repository" title="Open Repository"
    class="text-xl slidev-icon-btn opacity-50 !border-none !hover:text-white">
    <carbon-logo-gitlab />
  </a>
</div>

<!--
Here we talk about introduction
-->

---
theme: seriph
layout: center
class: text-center
---

# Linkedin users love attached document over wall-text

### How true is that? Let's find out!

---
theme: seriph
---

# How to create documents to be uploaded in LinkedIn?

<div grid="~ cols-2 gap-2" m="t-2">

<div>

Options that I could think of, are:

- **Microsoft Power Point** - the OG or well-known software for the job
- **Google Slides** - google tried to copy the OG
- what else?

</div>
<div p="12">
    <img src="https://assets.website-files.com/5a5e482ffab60f00019e3d8f/5e94b2f487cdb35b13d26c59_thumbnail%202.png" />
</div>

</div>

---
theme: seriph
layout: center
class: text-center
---

## Using Microsoft Power Point and Google Slides <br>means I have **no control** <br>over **content** I made and the **software** I used

---
theme: seriph
layout: center
---

### Quick search, I found [**reveal.js**](https://revealjs.com). It was not a good fit for me.

<br>

### Then @Tegar Imansyah, recommended me [**Slidev**](https://sli.dev).

---

# Code

After finished with setup[^1], I only need to put these content to create a slide

```md {1-3|4-6|7-9|10-12|all} twoslash
---
theme: serph 
---

# Slide 1

---
layout: center
---

# Slide 2

---

# Slide 3
```

<!-- Footer -->
[^1]: [Setup](https://sli.dev/guide/#create-locally)

<!-- Inline style -->
<style>
.footnotes-sep {
  @apply mt-5 opacity-10;
}
.footnotes {
  @apply text-sm opacity-75;
}
.footnote-backref {
  display: none;
}
</style>

---
layout: center
---

# What is the point of "writing" our own slide?

- Full ownership of the data we wrote
- I can save and backup to any git server (GitHub, Gitlab, or anything)
- I can update and download it anytime I want
- I don't have to struggle with finding which folder my slides was stored

---
layout: center
class: text-center text-3xl
---

Interested to learn more?

Keep in touch

<br>

[ridho@fbrns.co](mailto:ridho@fbrns.co)
