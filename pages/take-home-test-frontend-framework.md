---
class: text-center
---

# Take Home Test
# Breakdown Series
# #0

---
layout: center
class: text-center
---

## I decided to share the whole process of working on a take home test as a part of my own self documentation,

<br>

## and perhaps for someone else learning resource ;)

---
layout: center
class: text-center
---

Keywords of the test:
# Frontend

---
layout: center
---

When I am facing a frontend take home test, there are few points that I should confirm:

1. Is the UI design provided?
2. Are the whole pages/scenarios already covered?
3. What are the framework required?
4. Is there any API integration? 
5. What is the most difficult task?
6. How many hours I have til the due date?

Anyone want to add something?

---
layout: center
---

Let's take a worst case scenario:

# UI Design is not provided?

1. Open up Figma Community (figma.com/community)
2. Search the topic asked (e.g. E-Commerce, Pokemon, Dashboard)
3. Click on any design that fit the requirement
4. Slice it out (convert to JSX/TSX/HTML, your choice)
5. Use Tailwind because I am good at it, and don't have to look at MDN often
6. Use Tailwind Component like shadcn/ui

---
layout: center
---

# Pages/Scenarios are not all covered?

1. Use existing Figma Design to fill up the missing pages/scenarios
2. Make it simple. Complex pages/scenarios might cost the deliverability by deadline.

---
layout: center
---

# What are the framework required?

Why is it an important question?

- Because familiarity toward a framework or a library lead to a faster development, and the opposite.
- Spend a time to learn and do research on a library or framework you are not familiar with, before actually working on it

There will be a rare case when you are experienced using React, but got Angular required in your test case. Try to negotiate if you are allowed to use React, or confirm if Angular is the hard requirement.

---
layout: center
---

# Is there any API integration?

Most of cases, yes

But you have to confirm, these:

1. Is the API provided? Full-stack take home test might require you to create your own API
2. If it's a private API, does the credential or access already provided?
3. Any specific library required for fetching API?
4. Is the API documentation provided? (Postman, OpenAPI, Swagger, Readme, Curl, anything else)

---
layout: center
---

# What is the most difficult task?

1. Analyze the whole tasks
2. Classify each task: 
    - I have a similar code (has lowest weight)
    - I have no similar code but I can do it
    - need research (has highest weight)
3. Make it clear which task are mandatory and which one are optional or a bonus
4. Sort tasks by the mandatory and it's weight
5. If there are any task with `need research`, I suggest to finish the lower weight task as soon as possible, then start right away on doing the research. It's okay to not fully implement the task, but the goal is the ability to learn or research on something new and integrate it with the existing app.

---
layout: center
---

# How many hours I have til the due date?

To fully understand the due date, the amount of hours you could spend to work on it, is crucial. You have to define your own timeline on doing this take home test.

If you are currently working full-time, you might unable to work on it 8 hours a day. Maybe an effective of 4 hours in a day, and 8 hours or more in weekend.

You have to define which tasks should be finished by which date.

---
layout: center
---

# Let's use this "framework" on the other post

---
layout: center
class: text-center text-3xl
---

Interested to learn more?

Keep in touch

<br>

[ridho@fbrns.co](mailto:ridho@fbrns.co)
