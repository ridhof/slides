---
class: text-center
---

# Take Home Test
# Breakdown Series
# Kaleka, Frontend
# #1
---
layout: center
class: text-center
---

## I decided to share the whole process of working on a take home test as a part of my own self documentation,

<br>

## and perhaps for someone else learning resource ;)

---
layout: center
---

Keywords of the test:
# Frontend - NextJS - Leaflet

---
layout: center
---

# Framework I used for frontend test:

1. Is the UI design provided?
2. Are the whole pages/scenarios already covered?
3. What are the framework required?
4. Is there any API integration? 
5. What is the most difficult task?
6. How many hours I have til the due date?

---
layout: center
---

# Is the UI design provided?

#### Luckily, the UI design is provided in Figma.
#### So I don't have to find or explore other designs.

---
layout: center
---

# Are the whole pages/scenarios already covered?

1. It has small amount of pages, which not all of them requires interactivity.
2. The pages are clear enough, a page that showing a map and an about me page.
3. Scenarios are clear enough, showing coordinates in map and clicking on of the coordinate will redirect to other page with the coordinate shows up.

---
layout: center
---

# What are the framework required?

The options are:
- ReactJS, I'm okay with it but I am not that good with it
- NextJS, I'm more comfortable using it
- VueJS, I used it in the past, but not really confident to use it now

So I decided to use **NextJS** in this case

---
layout: center
---

# Is there any API integration?

Yes, there is an API integration.

1. **Is the API provided?** The API is provided
2. **If it's a private API, does the credential or access already provided?** It's a public API
3. **Any specific library required for fetching API?** No, I decided to use NextJS Fetch since I am using NextJS
4. **Is the API documentation provided?** No

It's my first time to hear [designer.mocky.io](https://designer.mocky.io), to mock an API so that any frontend test case can directly integrate to this service without having to deploy or running a backend service somewhere.

---
layout: center
---

# What is the most difficult task?

Breaking down the tasks:

1. I have to use any frontend framework that I've been months away not intensively using it
2. The UI design is provided, it means I have to slice it out instead of using existing component
3. I haven't make any map before, so I have to dedicate a time to research

<br/>

## Which lead to:

<br/>

1. Frontend framework, (2) I have no similar code but I can do it
2. Slice the UI design out using Tailwind, (2) I have no similar code but I can do it 
3. Integrate to API, (2) I have no similar code but I can do it
4. Map, (3) need research

---
layout: center
---

# How many hours I have til the due date?

- I received the test by Tuesday, but the due date is in Monday, next week
- But I will start by Wednesday
- I will include weekends to finish it up
- I have 6 days
- Since my other project is being halt, I plan to use my whole 8 hours for doing the test
- 6 days times to 8 hours = I have 48 hours

---
class: text-center
---

# Timeline

```mermaid
gantt
dateFormat DD-MM-YYYY

section Setup Project
Next Starter : sp1, 2024-02-01, 1d
Deployment : sp2, 2024-02-01, 1d

section Slicing Out
Navigation Component : so1, 2024-02-01, 1d
Banner Component : so2, after sp1, 1d
Map Component without interactivity : so3, after sp1, 1d
Home Page : so4, after so3, 1d
About Me Page : so5, after so3, 1d
Detail Page : s06, after so5, 1d

section Map Functionality
Draw circles on Map : mf1, after so3, 1d
Clickable circle : mf2, after mf1, 1d
Redirect clickable circle to new page : mf3, after mf2, 1d

section API Integration
Integrate to API : ai1, after mf2, 1d

section Refactoring
Code refactoring : rf1, after ai1, 1d
Write up documentation : rf2, after ai1, 1d
```

---
layout: center
---

# Lesson Learned

1. Integrating map is not easy, both setting it up and developing features on it. It's a worth decision if you want to be good at it.
2. React has many APIs, there are plenty APIs to manage state. But on this take home test I am only using useState because it can cover the test case. But at some point I have to learn the other APIs.
3. I am still unsure with the way to Integrate REST API, but the NextJS documentation is limited. So I need to learn through other references or open source codes.

---
layout: center
class: text-center
---

![https://i.ibb.co/PYdbH1W/kaleka-map-beranda.png](https://i.ibb.co/PYdbH1W/kaleka-map-beranda.png)

[kaleka.fbrns.co](https://kaleka.fbrns.co)

---
layout: center
class: text-center text-3xl
---

Interested to learn more?

Keep in touch

<br>

[ridho@fbrns.co](mailto:ridho@fbrns.co)
